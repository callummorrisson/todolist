# README #

Implementation of a basic ToDo list. Client side is angular 6. Server side is using express. Comms is handled by socket.io. Data is stored in MySQL.

# Setup #
on a box with nodejs and npm installed:
- in terminal/cmd navigate to the root directory of the application (directory containing app.js, packages.json, etc)
- run "npm install ."
- run "npm run build"
- run "node app.js"

- site should now be accessible at localhost:3000.

installation on Raspberry Pi:

* install mysql
    * add user 'ToDoList' with password 'Welcome1'
    * create database 'ToDoList'
* install chromium
* install node
* install npm
* install git
* use npm to install "forever" globally
* get latest from this repo
* run npm install in the root directory of this repo
* run 'npm build' in the root directory of this repo
    - may require that build some of the dependencies independently (eg. node-sass might need to be built for the arm processor)
* test that the code still runs "node app.js" and localhost:3000 is available

* in your autostart you will need these lines:
    - forever start [source path]/app.js
    - sleep 2
    - chromium --noerrdialogs --kiosk http://localhost:3000 --incognito