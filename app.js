const express = require('express');
const path = require('path');
const app = express();
app.use('/', express.static(path.join(__dirname, 'dist/todolist')));

const http = require('http').Server(app);
const io = require('socket.io')(http);
const dataAccess = require('./dataAccess');

/**
 * Wire up socket.io
 */
io.sockets.on('connection', function(socket) {

	socket.on("requestResync", function(data) {
		dataAccess.getAll(function(tasks) {
			socket.emit('tasksResync', tasks);
		});
		dataAccess.getAllCategories(function(categories) {
			socket.emit('categoriesResync', categories);
		});
	});

	socket.on("taskDone", function(data) {
		var taskId = data.taskId;

		dataAccess.taskComplete(taskId, function(task) {
      // resync all
			dataAccess.getAll(function(tasks) {
				io.sockets.emit('tasksResync', tasks);
			});
		});
	});

	socket.on("createTask", function(data) {
		dataAccess.createTask(data, function() {
			// resync all
			dataAccess.getAll(function(tasks) {
				io.sockets.emit('tasksResync', tasks);
			});
		});
	});

	socket.on("updateTask", function(data) {
		dataAccess.updateTask(data, function(task) {
				// resync all
        dataAccess.getAll(function(tasks) {
          io.sockets.emit('tasksResync', tasks);
        });
		});
	});

	socket.on("deleteTask", function(data) {
		var taskId = data.taskId;

		dataAccess.deleteTask(taskId, function(task) {
			// resync all
			dataAccess.getAll(function(tasks) {
				io.sockets.emit('tasksResync', tasks);
			});
		});
	});

	socket.on("createCategory", function(data) {
		dataAccess.createCategory(data, function() {
			// resync all
			dataAccess.getAllCategories(function(categories) {
				socket.emit('categoriesResync', categories);
			});
		});
	});

	socket.on("updateCategory", function(data) {
		dataAccess.updateCategory(data, function(category) {
      // resync all
      dataAccess.getAllCategories(function(categories) {
        socket.emit('categoriesResync', categories);
      });
		});
	});

	socket.on("deleteCategory", function(data) {
		var categoryId = data.categoryId;

		dataAccess.deleteCategory(categoryId, function() {
			// resync all
			dataAccess.getAll(function(tasks) {
				socket.emit('tasksResync', tasks);
			});
			dataAccess.getAllCategories(function(categories) {
				socket.emit('categoriesResync', categories);
			});
		});
	});

	socket.on("removeHistory", function(data) {
		dataAccess.deleteHistory(data.taskId, data.historyId, function(task) {
      // resync all
			dataAccess.getAll(function(tasks) {
				socket.emit('tasksResync', tasks);
			});
		});
	});

	socket.on("postponeTask", function(data) {
		dataAccess.postponeTask(data.taskId, data.time, function(task) {
      // resync all
      dataAccess.getAll(function(tasks) {
        socket.emit('tasksResync', tasks);
      });
		});
	});
});

http.listen(3000);
