/**
 * Methods for accessing data
 */
var Sequelize = require('sequelize'),
	connection = new Sequelize('ToDoList', 'ToDoList', 'Welcome1', {
		host: 'localhost',
		dialect: 'mysql',

		pool: {
			max: 5,
			min: 0,
			idle: 10000
		}
	});

var _mapToDto = function(task) {
	var task = task.dataValues;
	return {
		id: task.id,
		name: task.name,
		frequency: task.frequency,
		frequencyPeriod: task.frequencyPeriod,
		postponedUntil: task.postponedUntil,
		history: task.TaskHistories.map(history => {
			var history = history.dataValues;
			return {
				id: history.id,
				time: history.time
			};
		}),
		categoryId: task.CategoryId
	};
};

var Task = connection.define('Task', {
	name: {
		type: Sequelize.STRING,
		unique: true,
		allowNull: false
	},
	postponedUntil: { 
		type: Sequelize.DATE 
	},
	frequency: {
		type: Sequelize.DECIMAL(10,2)
	},
	frequencyPeriod: {
		type: Sequelize.STRING
	},
	isDeleted: {
		type: Sequelize.BOOLEAN,
		defaultValue: false
	}
});

var TaskHistory = connection.define('TaskHistory', {
		time: { 
			type: Sequelize.DATE, 
			defaultValue: Sequelize.NOW
		}
	}, 
	{
		timestamps: false,
		freezeTableName: true // maintain TaskHistory as the tablename
	});

var Category = connection.define('Category', {
	name: {
		type: Sequelize.STRING,
		unique: true,
		allowNull: false
	},
	isDeleted: {
		type: Sequelize.BOOLEAN,
		defaultValue: false
	}
});

TaskHistory.belongsTo(Task); 
Task.hasMany(TaskHistory); 

Task.belongsTo(Category); 
Category.hasMany(Task); 

connection.sync();

module.exports = {
	getAll: function (handler) {
			Task
			.findAll({ 
				where: {
					isDeleted: false
				},
				include: [ TaskHistory ]
			})
			.then(function(data) {
				var tasks = data.map(_mapToDto);
				handler(tasks);
			});
	},
	getAllCategories: function(handler) {
		Category
			.findAll({ where: { isDeleted: false }})
			.then(function(data) {
				var categories = data.map(function(category) {
					return {
						id: category.id,
						name: category.name
					}
				});

				handler(categories);
			});
	},
	taskComplete: function(taskId, handler) {
		var now = new Date();

		TaskHistory.create({
			TaskId: taskId,
			time: now
		})
		.then(function() {
			Task.findOne({ 
				where: {
					id: taskId,
					isDeleted: false
				},
				include: [ TaskHistory ]
			})
			.then(function(data) {
				data.update({ postponedUntil: null })
					.then(() => {});

				var task = _mapToDto(data);
				handler(task);
			});
		});
	},
	updateTask: function(details, handler) {
		Task.update({
			name: details.name,
			frequency: details.frequency,
			frequencyPeriod: details.frequencyPeriod,
			CategoryId: details.categoryId
		},
		{
			where: {
				id: details.id
			}
		})
		.then(function() {
			Task.findOne({ 
				where: {
					id: details.id,
					isDeleted: false
				},
				include: [ TaskHistory ] 
			})
			.then(function(data) {
				var task = _mapToDto(data);
				handler(task);
			});
		});
	},
	deleteTask: function(taskId, handler) {
		Task.update({
			isDeleted: true
		},
		{
			where: {
				id: taskId
			}
		})
		.then(handler);
	},
	deleteHistory: function(taskId, historyId, handler) {
		TaskHistory.destroy({
			where: {
				id: historyId,
				TaskId: taskId
			}
		})
		.then(function() {
			Task.findOne({ 
				where: {
					id: taskId,
					isDeleted: false
				},
				include: [ TaskHistory ] 
			})
			.then(function(data) {
				var task = _mapToDto(data);
				handler(task);
			});
		});
	},
	createTask: function(details, handler) {
		Task.create({
			name: details.name,
			frequency: details.frequency,
			frequencyPeriod: details.frequencyPeriod,
			CategoryId: details.categoryId
		})
		.then(handler);
	},
	createCategory: function(details, handler) {
		Category.create({
			name: details.name
		})
		.then(handler);
	},
	updateCategory: function(details, handler) {
		Category.update({
			name: details.name,
		},
		{
			where: {
				id: details.id
			}
		})
		.then(function() {
			Category.findOne({ 
				where: {
					id: details.id,
					isDeleted: false
				}
			})
			.then(function(data) {
				var category = {
					id: data.dataValues.id,
					name: data.dataValues.name
				};

				handler(category);
			});
		});
	},
	deleteCategory: function(categoryId, handler) {
		Category.update({
			isDeleted: true,
		},
		{
			where: {
				id: categoryId
			}
		})
		.then(function() {
			handler();
			Task.update({
				CategoryId: null
			},
			{
				where: {
					CategoryId: categoryId
				}
			})
			.then(handler);
		});
	},
	postponeTask: function(taskId, time, handler) {
		var now = new Date(),
			postponedUntil = new Date(now.getTime() + time);

		Task.update({
			postponedUntil: postponedUntil
		},
		{
			where: {
				id: taskId
			}
		})
		.then(function() {
			Task.findOne({ 
				where: {
					id: taskId,
					isDeleted: false
				},
				include: [ TaskHistory ] 
			})
			.then(function(data) {
				var task = _mapToDto(data);
				handler(task);
			});
		});
	}
}