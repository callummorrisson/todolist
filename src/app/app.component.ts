import { Component, OnInit } from '@angular/core';
import { AppService } from './services/app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  showAddMenu: boolean;
  showAddTask: boolean;
  showAddCategory: boolean;

  title = 'todolist';

  constructor(
    private _appService: AppService
  ) { }

  ngOnInit(): void {
    this._appService.showAddMenu.subscribe(val => this.showAddMenu = val);
    this._appService.showAddTask.subscribe(val => this.showAddTask = val);
    this._appService.showAddCategory.subscribe(val => this.showAddCategory = val);
  }
}
