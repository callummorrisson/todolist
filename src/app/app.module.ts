import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { ListContainerComponent } from './components/list-container/list-container.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { ListCategoryComponent } from './components/list-category/list-category.component';
import { environment } from '../environments/environment';
import { AddMenuComponent } from './components/dialogs/add-menu/add-menu.component';
import { AddTaskComponent } from './components/dialogs/add-task/add-task.component';
import { AddCategoryComponent } from './components/dialogs/add-category/add-category.component';
import { LightboxComponent } from './components/lightbox/lightbox.component';

const config: SocketIoConfig = { url: '', options: {} };

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ListContainerComponent,
    ListItemComponent,
    ListCategoryComponent,
    AddMenuComponent,
    AddTaskComponent,
    AddCategoryComponent,
    LightboxComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    SocketIoModule.forRoot(config)
  ],
  providers: [
    { provide: 'DataAccess', useClass: environment.dataAccessType }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
