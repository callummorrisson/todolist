import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/services/app.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { DataAccessService } from 'src/app/services/data-access/data-access.service';
import { Category } from 'src/app/models/category';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {
  form: FormGroup;

  constructor(
    private _appService: AppService,
    private _dataService: DataAccessService
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl('', Validators.required)
    });
  }

  lightboxClick(event: MouseEvent) {
    if (event.target == event.currentTarget) {
      this._appService.setShowAddCategory(false);
    }
  }

  cancelClicked() {
    this._appService.setShowAddCategory(false);
  }

  saveClicked() {
    if (this.form.valid) {
      const category = new Category();
      category.name = this.form.get('name').value;

      this._dataService.addCategory(category);

      this._appService.setShowAddCategory(false);
    }
  }

}
