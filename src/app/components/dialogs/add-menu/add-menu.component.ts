import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/services/app.service';

@Component({
  selector: 'app-add-menu',
  templateUrl: './add-menu.component.html',
  styleUrls: ['./add-menu.component.scss']
})
export class AddMenuComponent implements OnInit {

  constructor(
    private _appService: AppService
  ) { }

  ngOnInit() {
  }

  lightboxClick(event: MouseEvent) {
    if (event.target == event.currentTarget) {
      this._appService.setShowAddMenu(false);
    }
  }

  addTaskClick() {
    this._appService.setShowAddMenu(false);
    this._appService.setShowAddTask(true);
  }

  addCategoryClick() {
    this._appService.setShowAddMenu(false);
    this._appService.setShowAddCategory(true);
  }
}
