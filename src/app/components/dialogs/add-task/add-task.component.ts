import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/services/app.service';
import { DataAccessService } from 'src/app/services/data-access/data-access.service';
import { Category } from 'src/app/models/category';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Task } from 'src/app/models/task';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss']
})
export class AddTaskComponent implements OnInit {
  form: FormGroup;
  categories: Category[];

  constructor(
    private _appService: AppService,
    private _dataService: DataAccessService
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl('', Validators.required),
      categoryId: new FormControl(''),
      frequency: new FormControl('', Validators.required),
      frequencyPeriod: new FormControl('Seconds', Validators.required)
    });

    this._dataService.Categories.subscribe(categories => this.categories = categories);
  }

  lightboxClick(event: MouseEvent) {
    if (event.target === event.currentTarget) {
      this._appService.setShowAddTask(false);
    }
  }

  cancelClicked() {
    this._appService.setShowAddTask(false);
  }

  saveClicked() {
    if (this.form.valid) {
      const formValues = this.form.value;
      const task: Task = {
        id: null,
        name: formValues.name,
        categoryId: formValues.categoryId,
        frequency: formValues.frequency,
        frequencyPeriod: formValues.frequencyPeriod,
        postponedUntil: null,
        history: null
      };

      this._dataService.addTask(task);

      this._appService.setShowAddTask(false);
    }
  }

}
