import { Component, OnInit } from '@angular/core';
import { faSync, faToggleOff, faToggleOn, faPlus, faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';

import { DataAccessService } from 'src/app/services/data-access/data-access.service';
import { AppService } from 'src/app/services/app.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  
  faSync = faSync;
  faToggleOff = faToggleOff;
  faToggleOn = faToggleOn;
  faPlus = faPlus;
  faEye = faEye;
  faEyeSlash = faEyeSlash;

  showCategories: boolean;
  showAll: boolean;

  constructor(
    private _dataAccess: DataAccessService,
    private _appService: AppService
  ) { }

  ngOnInit() {
    this._appService.showCategories.subscribe(val => this.showCategories = val);
    this._appService.showAll.subscribe(val => this.showAll = val);
  }

  refresh() {
    this._dataAccess.refresh();
  }

  toggleShowAll() {
    this._appService.setShowAll(!this.showAll);
  }

  toggleShowCategories() {
    this._appService.setShowCategories(!this.showCategories);
  }

  addClicked() {
    this._appService.setShowAddMenu(true);
  }
}
