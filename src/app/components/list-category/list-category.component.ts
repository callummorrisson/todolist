import { Component, OnInit, Input } from '@angular/core';
import { Category } from 'src/app/models/category';
import { ComputedTask } from 'src/app/models/computed-task';

@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.scss']
})
export class ListCategoryComponent implements OnInit {

  @Input() category: Category;
  @Input() tasks: ComputedTask[] = [];

  isExpanded = false;

  constructor() { }

  ngOnInit() {
  }

  titleClicked() {
    this.isExpanded = !this.isExpanded;
  }

  getMainColor() {
    const task = this.tasks[0];

    if (!task) {
      return '#445';
    }

    const r = Math.min(task.urgency * 128, 220);
    const g = Math.min(Math.max(512 - (task.urgency * 128), 20), 220);
    const b = Math.min(task.urgency * 10, 20);

    return `rgb(${r}, ${g}, ${b})`;
  }

  getBorderColor() {
    const task = this.tasks[0];
    return !!task ? this.getDetailsColor() : '#DDD';
  }

  getDetailsColor() {
    const task = this.tasks[0];
    if (!task) {
      return '#334';
    }

    const r = Math.min(task.urgency * 128, 220) * 0.8;
    const g = Math.min(Math.max(512 - (task.urgency * 128), 20), 220) * 0.8;
    const b = Math.min(task.urgency * 10, 20)  * 0.8;

    return `rgb(${r}, ${g}, ${b})`;
  }

  getLineColor() {
    const task = this.tasks[0];
    if (!task) {
      return '#DDD';
    }

    const shade = task.urgency > 2 ? 255 : 0;

    return `rgb(${shade}, ${shade}, ${shade})`;
  }
}
