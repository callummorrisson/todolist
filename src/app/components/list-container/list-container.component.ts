import { Component, OnInit } from '@angular/core';
import { DataAccessService } from 'src/app/services/data-access/data-access.service';
import { ComputedTask } from 'src/app/models/computed-task';
import { AppService } from 'src/app/services/app.service';
import { Category } from 'src/app/models/category';

@Component({
  selector: 'app-list-container',
  templateUrl: './list-container.component.html',
  styleUrls: ['./list-container.component.scss']
})
export class ListContainerComponent implements OnInit {

  tasks: ComputedTask[] = [];
  categories: Category[] = [];

  private _showAll = false;
  private _allCategories: Category[] = [];
  showCategories = false;

  constructor(
    private _dataAccess: DataAccessService,
    private _appService: AppService
  ) { }


  ngOnInit() {
    this._appService.showAll.subscribe(x => this._showAll = x);
    this._appService.showCategories.subscribe(x => this.showCategories = x);

    this._dataAccess.Tasks.subscribe(tasks => this.updateTasks(tasks));
    this._dataAccess.Categories.subscribe(categories => this.updateCategories(categories));
  }

  updateTasks(updatedTasks: ComputedTask[]): any {

    const filteredTasks = updatedTasks.filter(x => this._showAll || x.urgency > x.readyUrgency);

    // update and add
    for (const task of filteredTasks) {
      const existingTask = this.tasks.find(x => x.id === task.id);
      if (!existingTask) {
        this.tasks.push(task);
        continue;
      }

      existingTask.updateFrom(task);
    }

    // remove
    for (let i = this.tasks.length - 1; i >= 0; i--) {
      const task = this.tasks[i];
      const existingTask = filteredTasks.find(x => x.id === task.id);
      if (!existingTask) {
        this.tasks.splice(i, 1);
      }
    }

    this.tasks.sort((a, b) => a.urgency === b.urgency ? 0 : a.urgency > b.urgency ? -1 : 1);
    this.updateVisibleCategories();
  }

  updateCategories(updatedCategories: Category[]) {

    this._allCategories = updatedCategories;

    this.updateVisibleCategories();
  }

  updateVisibleCategories() {

    const filteredCategories =
    this._allCategories.filter(category => this._showAll || this.tasks.some(task => task.categoryId === category.id));

    // update and add
    for (const category of filteredCategories) {
      const existingCategory = this.categories.find(x => x.id === category.id);
      if (!existingCategory) {
        this.categories.push(category);
        continue;
      }

      // todo category.updateFrom(...)
      existingCategory.name = category.name;
    }

    // remove
    for (let i = this.categories.length - 1; i >= 0; i--) {
      const category = this.categories[i];
      const existingCategory = filteredCategories.find(x => x.id === category.id);
      if (!existingCategory) {
        this.categories.splice(i, 1);
      }
    }

    this.categories.sort((a, b) => {
      const taskAIndex = this.tasks.findIndex(x => x.categoryId === a.id);
      const taskBIndex = this.tasks.findIndex(x => x.categoryId === b.id);

      return taskAIndex === taskBIndex ? 0 : taskAIndex > taskBIndex ? -1 : 1;
    });
  }

  getTasksForCategory(category: Category): ComputedTask[] {
    return this.tasks.filter(task => task.categoryId === category.id);
  }
}
