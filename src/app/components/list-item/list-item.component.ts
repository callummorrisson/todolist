import { Component, OnInit, Input } from '@angular/core';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { DataAccessService } from 'src/app/services/data-access/data-access.service';
import { ComputedTask } from '../../models/computed-task';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {
  faCheck = faCheck;

  @Input()
  task: ComputedTask;

  isExpanded = false;

  constructor(
    private _dataService: DataAccessService
  ) { }

  ngOnInit() {
  }

  titleClicked() {
    this.isExpanded = !this.isExpanded;
  }

  doneClicked() {
    this._dataService.taskDone(this.task.id);
  }

  shouldDisplay() {
    return this.task.urgency > this.task.readyUrgency;
  }

  getMainColor() {
    if (this.task.urgency < 0) {
      return '#445';
    }

    const r = Math.min(this.task.urgency * 128, 220);
    const g = Math.min(Math.max(512 - (this.task.urgency * 128), 20), 220);
    const b = Math.min(this.task.urgency * 10, 20);

    return `rgb(${r}, ${g}, ${b})`;
  }

  getBorderColor() {
    return this.task.urgency > 0 ? this.getDetailsColor() : '#DDD';
  }

  getDetailsColor() {
    if (this.task.urgency < 0) {
      return '#334';
    }

    const r = Math.min(this.task.urgency * 128, 220) * 0.8;
    const g = Math.min(Math.max(512 - (this.task.urgency * 128), 20), 220) * 0.8;
    const b = Math.min(this.task.urgency * 10, 20)  * 0.8;

    return `rgb(${r}, ${g}, ${b})`;
  }

  getLineColor() {
    if (this.task.urgency < 0) {
      return '#DDD';
    }

    const shade = this.task.urgency > 2 ? 255 : 0;

    return `rgb(${shade}, ${shade}, ${shade})`;
  }
}
