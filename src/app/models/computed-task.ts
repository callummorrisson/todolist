import { Task } from './task';
import { TaskHistory } from './task-history';

export class ComputedTask implements Task {
  id: number;
  name: string;
  categoryId?: number;
  frequency: number;
  frequencyPeriod: string;
  postponedUntil?: Date;
  history: TaskHistory[];

  constructor(
    task: Task,
    frequencyMilliseconds: number,
    urgency: number,
    lastCompleted: Date,
    nextDue: Date,
    counterText: string,
    readyUrgency: number
    ) {
      this.id = task.id;
      this.name = task.name;
      this.categoryId = task.categoryId;
      this.frequency = task.frequency;
      this.frequencyPeriod = task.frequencyPeriod;
      this.postponedUntil = task.postponedUntil;
      this.history = task.history;

      this.frequencyMilliseconds = frequencyMilliseconds;
      this.lastCompleted = lastCompleted;
      this.nextDue = nextDue;
      this.urgency = urgency;
      this.counterText = counterText;
      this.readyUrgency = readyUrgency;
    }

  frequencyMilliseconds: number;
  lastCompleted?: Date;
  nextDue?: Date;
  urgency: number;
  counterText: string;
  readyUrgency: number;

  updateFrom(task: ComputedTask): any {
    this.frequencyMilliseconds = task.frequencyMilliseconds;
    this.lastCompleted = task.lastCompleted;
    this.nextDue = task.nextDue;
    this.urgency = task.urgency;
    this.counterText = task.counterText;
    this.readyUrgency = task.readyUrgency;
  }
}
