export interface TaskHistory {
  id: number;
  time: Date;
}
