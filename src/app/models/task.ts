import { TaskHistory } from './task-history';

export interface Task {
  id: number;
  name: string;
  categoryId?: number;
  frequency: number;
  frequencyPeriod: string;
  postponedUntil?: Date;
  history: TaskHistory[];
}
