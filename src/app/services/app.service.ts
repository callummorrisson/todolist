import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {from} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {
 
  private _showCategories: Subject<boolean>;
  private _showAll: Subject<boolean>;
  private _showAddMenu: Subject<boolean>;
  private _showAddTask: Subject<boolean>;
  private _showAddCategory: Subject<boolean>;

  showCategories: Observable<boolean>;
  showAll: Observable<boolean>;
  showAddMenu: Observable<boolean>;
  showAddTask: Observable<boolean>;
  showAddCategory: Observable<boolean>;
  
  constructor() { 
    this._showCategories = new Subject<boolean>();
    this._showAll = new Subject<boolean>();
    this._showAddMenu = new Subject<boolean>();
    this._showAddTask = new Subject<boolean>();
    this._showAddCategory = new Subject<boolean>();

    this.showCategories = this._showCategories.asObservable();
    this.showAll = this._showAll.asObservable();
    this.showAddMenu = this._showAddMenu.asObservable();
    this.showAddTask = this._showAddTask.asObservable();
    this.showAddCategory = this._showAddCategory.asObservable();
  }

  setShowCategories(val: boolean): void {
    this._showCategories.next(val);
  }

  setShowAll(val: boolean): void {
    this._showAll.next(val);
  }

  setShowAddMenu(val: boolean): void {
    this._showAddMenu.next(val);
  }

  setShowAddCategory(val: boolean): any {
    this._showAddCategory.next(val);
  }
  setShowAddTask(val: boolean): any {
    this._showAddTask.next(val);
  }
}
