import { TestBed, inject } from '@angular/core/testing';

import { ComputedTaskService } from './computed-task.service';

describe('ComputedTaskService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ComputedTaskService]
    });
  });

  it('should be created', inject([ComputedTaskService], (service: ComputedTaskService) => {
    expect(service).toBeTruthy();
  }));
});
