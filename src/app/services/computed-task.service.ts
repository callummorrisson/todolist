import { Injectable } from '@angular/core';

import { ComputedTask } from '../models/computed-task';
import { Task } from '../models/task';

@Injectable({
  providedIn: 'root'
})
export class ComputedTaskService {
  createComputedTask(task: Task): ComputedTask {
    const parsedTask: Task = {
      ...task,
      postponedUntil: task.postponedUntil
        ? new Date(task.postponedUntil)
        : null,
      history: task.history.map(x => ({ id: x.id, time: new Date(x.time) }))
    };

    const frequencyMilliseconds: number = this.getFrequencyMilliseconds(
      parsedTask
    );
    const lastCompleted: Date = this.getLastCompleted(parsedTask);
    const nextDue: Date = this.getNextDue(
      parsedTask,
      lastCompleted,
      frequencyMilliseconds
    );
    const urgency: number = this.getUrgency(nextDue, frequencyMilliseconds);
    const counterText: string = this.getCounterText(nextDue);
    const readyUrgency: number = this.getReadyUrgency(frequencyMilliseconds);

    return new ComputedTask(
      parsedTask,
      frequencyMilliseconds,
      urgency,
      lastCompleted,
      nextDue,
      counterText,
      readyUrgency
    );
  }

  getReadyUrgency(frequencyMilliseconds): number {
    const days = frequencyMilliseconds / 1000 / 60 / 60 / 24;
    // todo this needs to be refined
    return 0.000151 * days + -0.152;
  }

  getUrgency(nextDue: Date, frequencyMilliseconds: number): any {
    let urgency = Number.MAX_SAFE_INTEGER;

    if (nextDue) {
      const now = Date.now();
      const nextDueMilliseconds = nextDue.getTime();

      // < 0 is not ready
      // 0 is ready
      // > 0 is a multiple of how many "frequencies" we're overdue
      urgency =
        (now - (nextDueMilliseconds - frequencyMilliseconds)) /
          frequencyMilliseconds -
        1;
    }

    return urgency;
  }

  getNextDue(
    task: Task,
    lastCompleted: Date,
    frequencyMilliseconds: number
  ): any {
    const nextDue = lastCompleted
      ? new Date(lastCompleted.getTime() + frequencyMilliseconds)
      : null;

    return task.postponedUntil || nextDue;
  }

  getLastCompleted(task: Task): Date {
    const lastPerformed = !!task.history.length
      ? new Date(Math.max.apply(Math, task.history.map(h => h.time.getTime())))
      : null;

    return lastPerformed;
  }

  getFrequencyMilliseconds(task: Task): number {
    let frequencyMilliseconds = task.frequency;

    switch (task.frequencyPeriod) {
      case 'Weeks':
        frequencyMilliseconds = frequencyMilliseconds * 7;
      case 'Days':
        frequencyMilliseconds = frequencyMilliseconds * 24;
      case 'Hours':
        frequencyMilliseconds = frequencyMilliseconds * 60;
      case 'Minutes':
        frequencyMilliseconds = frequencyMilliseconds * 60;
      case 'Seconds':
        frequencyMilliseconds = frequencyMilliseconds * 1000;
        break;
      default:
        frequencyMilliseconds = 0;
    }

    return frequencyMilliseconds;
  }

  getCounterText(nextDue: Date) {
    if (!nextDue) {
      return 'never performed!';
    }

    const now = Date.now();
    const nextDueTime = nextDue.getTime();
    let timeLeft = Math.abs(now - nextDueTime);
    const seconds = Math.round((timeLeft /= 1000) % 60);
    const minutes = Math.round((timeLeft /= 60) % 60);
    const hours = Math.round((timeLeft /= 60) % 24);
    const days = Math.round(timeLeft / 24);

    let text = nextDueTime > now ? '' : 'overdue ';

    if (Math.abs(days) > 0) {
      text += `${days} day(s)`;
    } else if (Math.abs(hours) > 0) {
      text += `${hours} hour(s)`;
    } else if (Math.abs(minutes) > 0) {
      text += `${minutes} minute(s)`;
    } else {
      text += `${seconds} second(s)`;
    }

    return text;
  }
}
