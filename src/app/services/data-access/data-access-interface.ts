import { Observable } from 'rxjs';
import { Category } from '../../models/category';
import { Task } from '../../models/task';


export interface IDataAccess {
  Tasks: Observable<Task[]>;
  Categories: Observable<Category[]>;

  refresh: () => void;

  taskDone: (taskId: number) => void;
  deleteHistory: (taskId: number, historyId: number) => void;

  addTask: (task: Task) => void;
  updateTask: (task: Task) => void;
  deleteTask: (taskId: number) => void;

  addCategory: (category: Category) => void;
  updateCategory: (category: Category) => void;
  deleteCategory: (categoryId: number) => void;
}