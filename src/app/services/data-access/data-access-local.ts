import { Task } from '../../models/task';
import { Category } from '../../models/category';
import { IDataAccess } from './data-access-interface';
import { Observable, Subject, BehaviorSubject } from 'rxjs';

export class DataAccess implements IDataAccess {

  private tasks: Task[];
  private categories: Category[];

  private _tasksSubject: BehaviorSubject<Task[]>;
  private _categoriesSubject: BehaviorSubject<Category[]>;

  Tasks: Observable<Task[]>;
  Categories: Observable<Category[]>;

  constructor() {
    this._tasksSubject = new BehaviorSubject<Task[]>([]);
    this._categoriesSubject = new BehaviorSubject<Category[]>([]);

    this.Tasks = this._tasksSubject.asObservable();
    this.Categories = this._categoriesSubject.asObservable();

    this.init();

  }

  init() {
    const taskJson = localStorage.getItem('ToDoListTasks');
    const categoryJson = localStorage.getItem('ToDoListCategories');

    if (!taskJson || !categoryJson) {
      this.tasks = [
        {
          id: 1,
          name: 'Seed Entry (5s)',
          categoryId: 1,
          frequency: 5,
          frequencyPeriod: 'Seconds',
          postponedUntil: null,
          history: [
            {
              id: 1,
              time: new Date()
            }
          ]
        }
      ];

      this.categories = [
        {
          id: 1,
          name: 'Seed Category'
        }
      ];

      this.saveData();

    } else {
      this.tasks = JSON.parse(taskJson);
      // cleanup dates
      this.tasks.forEach(t => {
        if (t.postponedUntil) {
          t.postponedUntil = new Date(t.postponedUntil);
        }
        if (t.history) {
          t.history.forEach(h => h.time = new Date(h.time));
        }
      });

      this.categories = JSON.parse(categoryJson);

      this.refresh();
    }
  }

  saveData() {
    localStorage.setItem('ToDoListTasks', JSON.stringify(this.tasks));
    localStorage.setItem('ToDoListCategories', JSON.stringify(this.categories));

    this.refresh();
  }

  refresh() {
    this._tasksSubject.next(this.tasks);
    this._categoriesSubject.next(this.categories);
  }

  taskDone(taskId: number) {
    const task = this.tasks.find((x) => x.id === taskId);
    const maxHistoryId = Math.max.apply(Math, task.history.map((x) => x.id));
    const newHistory = {
        id: maxHistoryId + 1,
        time: new Date()
      };

    task.history.push(newHistory);
    this.saveData();
  }

  deleteHistory(taskId: number, taskHistoryId: number) {
    const task = this.tasks.find((x) => x.id === taskId);
    const index = task.history.findIndex((x) => x.id === taskHistoryId);

    task.history.splice(index, 1);
    this.saveData();
  }

  addTask(task: Task) {
    const maxTaskId = Math.max.apply(Math, this.tasks.map((x) => x.id));
    const newTask = {
        id: maxTaskId + 1,
        name: task.name,
        categoryId: task.categoryId,
        frequency: task.frequency,
        frequencyPeriod: task.frequencyPeriod,
        history: []
      };

    this.tasks.push(newTask);
    this.saveData();
  }

  updateTask(task: Task) {
    const taskId = task.id;
    const targetTask = this.tasks.find((x) => x.id === taskId);

    targetTask.name = task.name;
    targetTask.categoryId = task.categoryId;
    targetTask.frequency = task.frequency;
    targetTask.frequencyPeriod = task.frequencyPeriod;
    this.saveData();
  }

  deleteTask(taskId: number) {
    const index = this.tasks.findIndex((x) => x.id === taskId);

    this.tasks.splice(index, 1);
    this.saveData();
  }

  addCategory(category: Category) {
    const maxCategoryId = Math.max.apply(Math, this.categories.map((x) => x.id));
    const newCategory = {
      id: maxCategoryId + 1,
      name: category.name
    };

    this.categories.push(newCategory);
    this.saveData();
  }

  updateCategory(category: Category) {
    const categoryId = category.id;
    const targetCategory = this.categories.find((x) => x.id === categoryId);

    targetCategory.name = category.name;
    this.saveData();
  }

  deleteCategory(categoryId: number) {
    const index = this.categories.findIndex((x) => x.id === categoryId);

    this.categories.splice(index, 1);
    this.tasks
      .filter((x) => x.categoryId === categoryId)
      .forEach((x) => x.categoryId = null);

    this.saveData();
  }
}
