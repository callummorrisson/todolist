import { Observable, BehaviorSubject } from 'rxjs';
import { Socket } from 'ngx-socket-io';

import { Category } from '../../models/category';
import { Task } from '../../models/task';
import { IDataAccess } from './data-access-interface';
import { Injectable } from '@angular/core';

@Injectable()
export class DataAccess implements IDataAccess {
  Tasks: Observable<Task[]>;
  Categories: Observable<Category[]>;

  constructor(private _socket: Socket) {
    const tasksSubject = new BehaviorSubject<Task[]>([]);
    const categoriesSubject = new BehaviorSubject<Category[]>([]);

    this.Tasks = tasksSubject.asObservable();
    this.Categories = categoriesSubject.asObservable();

    this._socket.fromEvent<Task[]>('tasksResync').subscribe(x => tasksSubject.next(x));
    this._socket.fromEvent<Category[]>('categoriesResync').subscribe(x => categoriesSubject.next(x));
    this.refresh();
  }

  refresh(): void {
    this._socket.emit('requestResync');
  }

  taskDone(taskId: number): void {
    this._socket.emit('taskDone', { taskId });
  }

  deleteHistory(taskId: number, historyId: number): void {
    this._socket.emit('removeHistory', { taskId, historyId });
  }

  addTask(task: Task): void {
    this._socket.emit('createTask', task);
  }

  updateTask(task: Task): void {
    this._socket.emit('updateTask', task);
  }

  deleteTask(taskId: number): void {
    this._socket.emit('deleteTask', { taskId });
  }

  addCategory(category: Category): void {
    this._socket.emit('createCategory', category);
  }

  updateCategory(category: Category): void {
    this._socket.emit('createCategory', category);
  }

  deleteCategory(categoryId: number): void {
    this._socket.emit('deleteCategory', { categoryId });
  }
}
