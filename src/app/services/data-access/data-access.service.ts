import { Injectable, Inject } from '@angular/core';
import { Observable, interval, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { Category } from '../../models/category';
import { Task } from '../../models/task';
import { IDataAccess } from './data-access-interface';
import { ComputedTask } from '../../models/computed-task';
import { ComputedTaskService } from '../computed-task.service';

@Injectable({
  providedIn: 'root'
})
export class DataAccessService {
  Tasks: Observable<ComputedTask[]>;
  Categories: Observable<Category[]>;
  private _tasks: Task[];

  constructor(
    @Inject('DataAccess')private _dataAccess: IDataAccess,
    taskService: ComputedTaskService
  ) {
    this.Tasks = combineLatest([interval(1000), _dataAccess.Tasks])
      .pipe(map(([_, tasks]: any) => tasks.map(task => taskService.createComputedTask(task))));

    this.Categories = _dataAccess.Categories;
  }

  refresh(): void {
    this._dataAccess.refresh();
  }

  addCategory(category: Category): void {
    this._dataAccess.addCategory(category);
  }

  addTask(task: Task): void {
    this._dataAccess.addTask(task);
  }

  taskDone(taskId: number): any {
    this._dataAccess.taskDone(taskId);
  }
}
