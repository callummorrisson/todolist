export function safeNumber(val: string | number): number | null {
  if (typeof val === 'number') {
    return val;
  }
  const parsed = parseInt(val, 10);
  return parsed === NaN ? null : parsed;
}
