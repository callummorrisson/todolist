import { DataAccess } from '../app/services/data-access/data-access-server';

export const environment = {
  production: true,
  dataAccessType: DataAccess
};
